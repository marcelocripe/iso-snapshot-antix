<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="tr">
<context>
    <name>Batchprocessing</name>
    <message>
        <location filename="batchprocessing.cpp" line="44"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="45"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Şu an kullanımdaki çekirdek, seçilen sıkıştırma algoritmasını desteklemiyor, lütfen yapılandırma dosyasını düzenleyin ve farklı bir algoritma seçin.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MX Snapshot</source>
        <translation>MX Sistem Anlık Görüntüsü</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Optional customization</source>
        <translation>İsteğe bağlı özelleştirme</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Boot options:</source>
        <translation>Önyükleme seçenekleri:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>Release date:</source>
        <translation>Sürüm tarihi:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Project name:</source>
        <translation>Proje adı:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Release version:</source>
        <translation>Sürüm:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <source>Release codename:</source>
        <translation>Sürü kod adı:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Anlık Görüntü, çalışma sisteminizin depolama veya dağıtım için kullanabileceğiniz önyüklenebilir bir görüntüsünü (ISO) oluşturan bir programdır. O bir yandan çalışırken, siz de basit uygulamalarla çalışmaya devam edebilirsiniz.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>/ (root) ve /home bölümlerinde kullanılan alan:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <source>Location and ISO name</source>
        <translation>Konum ve ISO adı</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="188"/>
        <source>Snapshot location:</source>
        <translation>Sistem Anlık Görüntüsü konumu:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>Select a different snapshot directory</source>
        <translation>Anlık görüntü için başka bir dizin seç</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <location filename="mainwindow.cpp" line="328"/>
        <source>Snapshot name:</source>
        <translation>Sistem Anlık Görüntüsü Adı:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="mainwindow.ui" line="370"/>
        <source>TextLabel</source>
        <translation>Metin Etiketi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /etc/mx-snapshot-exclude.list.</source>
        <translation>Aşağıdaki ortak seçimleri işaretleyerek veya /etc/mx-snapshot-exclude.list.&apos;i doğrudan düzenlemek için düğmeyi tıklayarak belli dizinleri de hariç tutabilirsiniz.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="395"/>
        <source>Pictures</source>
        <translation>Resimler</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <source>Music</source>
        <translation>Müzik</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <source>All of the above</source>
        <translation>Yukarıdakilerin Tümü</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="426"/>
        <source>Documents</source>
        <translation>Belgeler</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <source>Downloads</source>
        <translation>İndirilenler</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <source>Desktop</source>
        <translation>Masaüstü</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="447"/>
        <source>Videos</source>
        <translation>Videolar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <source>exclude network configurations</source>
        <translation>ağ yapılandırmalarını hariç tut</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>Networks</source>
        <translation>Ağlar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="505"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Kullanıcı hesaplarını tutarak (kişisel kullanım için)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="515"/>
        <source>Type of snapshot:</source>
        <translation>Anlık Görüntü Tipi:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the MX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Bu seçenek &amp;quot;demo&amp;quot; ve &amp;quot;root&amp;quot; şifrelerini MX Linux varsayılanlarına getirir -resetler- ve oluşturulmuş herhangi bir  kişisel hesabı kopyalamaz.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="538"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Kullanıcı hesaplarını sıfırlayarak (başkalarına vermek için)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>Edit Exclusion File</source>
        <translation>Hariç Bırakılacaklar Dosyasını Düzenle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="577"/>
        <source>sha512</source>
        <translation>sha512</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="584"/>
        <source>Options:</source>
        <translation>Seçenekler:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="604"/>
        <source>md5</source>
        <translation>md5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="611"/>
        <source>Calculate checksums:</source>
        <translation>Sağlama toplamlarını hesapla:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="618"/>
        <source>ISO compression scheme:</source>
        <translation>ISO sıkıştırma biçimi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="733"/>
        <source>About this application</source>
        <translation>Uygulama hakkında</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <source>About...</source>
        <translation>Hakkında...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="743"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="759"/>
        <source>Quit application</source>
        <translation>Uygulamadan çık</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="762"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="769"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <source>Next</source>
        <translation>Sonraki</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="849"/>
        <source>Back</source>
        <translation>Geri</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="866"/>
        <source>Display help </source>
        <translation>Yardımı görüntüle</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="869"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="876"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Snapshot</source>
        <translation>Sistem Anlık Görüntüsü</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>fastest, worst compression</source>
        <translation>en hızlı, en kötü sıkıştırma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>fast, worse compression</source>
        <translation>hızlı, kötü sıkıştırma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>slow, better compression</source>
        <translation>yavaş, daha iyi sıkıştırma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>best compromise</source>
        <translation>en iyi uzlaşma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="166"/>
        <source>slowest, best compression</source>
        <translation>en yavaş, en iyi sıkıştırma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="193"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Anlık Görüntü&apos;nün bulunduğu %1 &apos;deki boş alan:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Boş alan, / ve / home /&apos;dan sıkıştırılmış veriyi kaydetmek için yeterli olmalıdır.

Gerekirse, önceki anlık görüntüleri kaldırarak
daha fazla kullanılabilir alan oluşturabilirsiniz:
%1 anlık görüntüler %2 disk alanı kaplıyor.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <location filename="mainwindow.cpp" line="207"/>
        <source>Installing </source>
        <translation>Kuruluyor</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Please wait.</source>
        <translation>Lütfen bekleyin.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Lütfen bekleyin. Kullanılan disk alanı hesaplanıyor...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="312"/>
        <location filename="mainwindow.cpp" line="340"/>
        <location filename="mainwindow.cpp" line="369"/>
        <location filename="mainwindow.cpp" line="373"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="313"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation>Çıktı dosyası %1 zaten var. Lütfen başka bir dosya adı kullanın veya mevcut dosyayı silin.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="320"/>
        <source>Settings</source>
        <translation>Ayarlar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="325"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Anlık Görüntü şu ayarları kullanacak:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="327"/>
        <source>- Snapshot directory:</source>
        <translation>- Ekran görüntüsü dizini:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="328"/>
        <source>- Kernel to be used:</source>
        <translation>- Kullanılacak Çekirdek:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="341"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Şu an kullanımdaki çekirdek, seçilen sıkıştırma algoritmasını desteklemiyor, lütfen yapılandırma dosyasını düzenleyin ve farklı bir algoritma seçin.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="348"/>
        <source>Final chance</source>
        <translation>Son şans</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="350"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Anlık Görüntü, artık çalışan sisteminizden bir ISO oluşturmak için gereken tüm bilgilere sahip.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Kurulu sistemin boyutuna ve bilgisayarınızın kapasitesine bağlı olarak işlem biraz zaman alacaktır.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>OK to start?</source>
        <translation>Başlamak için hazır mısınız?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Shutdown computer when done.</source>
        <translation>Tamamlandığında bilgisayarı kapat.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="369"/>
        <source>Could not create working directory. </source>
        <translation>Çalışma dizini oluşturulamadı.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="373"/>
        <source>Could not create temporary directory. </source>
        <translation>Geçici dizin oluşturulamadı.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="381"/>
        <source>Output</source>
        <translation>Çıktı</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>Edit Boot Menu</source>
        <translation>Boot Menüsünü Düzenle</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Program şimdi çalışma dizinindeki herhangi bir dosyayı düzenleyebilmenize imkan sağlamak için duraklatılacak. Önyükleme menüsünü düzenlemek için Evet&apos;i ; ya da bu adımı atlamak ve anlık görüntüyü oluşturmaya devam etmek için Hayır&apos;ı seçin.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <source>About %1</source>
        <translation>Hakkında %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="491"/>
        <source>Version: </source>
        <translation>Sürüm:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>MX Linux için, çalışmakta olan sistemden bir canlı-CD imaj dosyası oluşturan program</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Telif Hakkı (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="496"/>
        <source>%1 License</source>
        <translation>%1 Telif Hakkı</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="510"/>
        <source>%1 Help</source>
        <translation>%1 Yardım</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="518"/>
        <source>Select Snapshot Directory</source>
        <translation>Anlık Görüntü Dizini Seçin</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="540"/>
        <source>Confirmation</source>
        <translation>Onay</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="540"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Uygulamadan çıkmak istediğinizden emin misiniz?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>License</source>
        <translation>Ruhsat</translation>
    </message>
    <message>
        <location filename="about.cpp" line="53"/>
        <location filename="about.cpp" line="62"/>
        <source>Changelog</source>
        <translation>Değişim günlüğü</translation>
    </message>
    <message>
        <location filename="about.cpp" line="54"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Kapat</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="67"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Program, yapımı duraklatacak ve metin düzenleyicinizde önyükleme menüsünü açacaktır. </translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Çalışan bir sistemden canlı CD oluşturmak için kullanılan araç</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use CLI only</source>
        <translation>Yalnızca CLI kullan</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>Output directory</source>
        <translation>Çıktı dizini</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="100"/>
        <source>path</source>
        <translation>yol</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Output filename</source>
        <translation>Çıktı dosya adı</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>name</source>
        <translation>ad</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Öntanımlı çalışan çekirdek dışında kullanmak için farklı bir çekirdeği adlandırın, &apos;uname -r&apos; ile döndürülen biçimi kullanın</translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>Or the full path: %1</source>
        <translation>Veya tam yol: %1 </translation>
    </message>
    <message>
        <location filename="main.cpp" line="81"/>
        <source>version, or path</source>
        <translation>sürüm, veya yol</translation>
    </message>
    <message>
        <location filename="main.cpp" line="83"/>
        <source>Compression level options.</source>
        <translation>Sıkıştırma düzeyi seçenekleri</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Use quotes: &quot;-Xcompression-level &lt;level&gt;&quot;, or &quot;-Xalgorithm &lt;algorithm&gt;&quot;, or &quot;-Xhc&quot;, see mksquashfs man page</source>
        <translation>Seçenekleri kullanın: &quot;-Xcompression-level &lt;level&gt;&quot;, veya &quot;-Xalgorithm &lt;algorithm&gt;&quot;, veya &quot;-Xhc&quot;, mksquashfs kılavuz sayfasına bakın</translation>
    </message>
    <message>
        <location filename="main.cpp" line="86"/>
        <source>&quot;option&quot;</source>
        <translation>&quot;seçenek&quot;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="89"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Aylık bir anlık görüntü oluşturun, ISO adına &apos; ay &apos; adı ekleyin, kullanılan alan hesaplamasını atlayın</translation>
    </message>
    <message>
        <location filename="main.cpp" line="90"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Bu seçenek, hesap sıfırlamalarını ve sıkıştırmayı varsayılanlara ayarlar, bu öğeleri değiştiren değişkenler yoksayılır</translation>
    </message>
    <message>
        <location filename="main.cpp" line="93"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Elde edilen ISO dosyası için sağlama toplamlarını hesaplamayın </translation>
    </message>
    <message>
        <location filename="main.cpp" line="95"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Preempt_rt çekirdeklerinde sağlama toplamı hesaplamayla ilgili sorunu düzeltme seçeneği</translation>
    </message>
    <message>
        <location filename="main.cpp" line="96"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Kullanıcı hesaplarını sıfırlayarak (başkalarına vermek için)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="97"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Elde edilen ISO dosyası için sağlama toplamlarını hesaplayın</translation>
    </message>
    <message>
        <location filename="main.cpp" line="99"/>
        <source>Skip calculating free space to see if the resulting ISO will fit</source>
        <translation>Oluşturulan ISO&apos;nun sığıp sığmayacağını görmek için boş alan hesaplamayı atlayın</translation>
    </message>
    <message>
        <location filename="main.cpp" line="100"/>
        <source>Work directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="main.cpp" line="102"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Ana klsörleri hariç tutun, geçerli seçimler:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Hariç tutmak istediğiniz her öğe için seçeneği bir kez kullanın</translation>
    </message>
    <message>
        <location filename="main.cpp" line="105"/>
        <source>one item</source>
        <translation>bir öğe</translation>
    </message>
    <message>
        <location filename="main.cpp" line="107"/>
        <source>Compression format, valid choices: </source>
        <translation>Sıkıştırma biçimi, geçerli seçimler:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="108"/>
        <source>format</source>
        <translation>biçim</translation>
    </message>
    <message>
        <location filename="main.cpp" line="109"/>
        <source>Shutdown computer when done.</source>
        <translation>Tamamlandığında bilgisayarı kapat.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="128"/>
        <location filename="main.cpp" line="138"/>
        <location filename="main.cpp" line="176"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Görünüşe göre root olarak girmişsiniz, lütfen çıkın ve bu programı kullanmak için normal kullanıcı olarak girin. </translation>
    </message>
    <message>
        <location filename="main.cpp" line="150"/>
        <location filename="main.cpp" line="184"/>
        <source>version:</source>
        <translation>sürüm:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="158"/>
        <source>You must run this program as root.</source>
        <translation>Bu programı root olarak çalıştırmalısınız.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="167"/>
        <source>Snapshot</source>
        <translation>Sistem Anlık Görüntüsü</translation>
    </message>
    <message>
        <location filename="main.cpp" line="175"/>
        <location filename="main.cpp" line="259"/>
        <location filename="settings.cpp" line="222"/>
        <location filename="settings.cpp" line="235"/>
        <location filename="settings.cpp" line="633"/>
        <location filename="settings.cpp" line="723"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="main.cpp" line="253"/>
        <location filename="main.cpp" line="255"/>
        <location filename="settings.cpp" line="230"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Şu an kullanılan çekirdek squashfs dosya sistemini desteklemediği için devam edilemiyor.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>Çalışma dizini oluşturulamadı.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="109"/>
        <source>Could not create temp directory. </source>
        <translation>Geçici dizin oluşturulamadı.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="217"/>
        <source>Could not find a usable kernel</source>
        <translation>Kullanılabilir bir çekirdek bulunamadı </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="346"/>
        <source>Used space on / (root): </source>
        <translation>/ (root) &apos;daki Kullanılan Alan:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>estimated</source>
        <translation>Tahminen</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="360"/>
        <source>Used space on /home: </source>
        <translation>/home &apos;daki Kullanılan Alan:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="411"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Anlık Görüntü&apos;nün bulunduğu %1 &apos;deki boş alan:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="415"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Boş alan, / ve / home /&apos;dan sıkıştırılmış veriyi kaydetmek için yeterli olmalıdır.

Gerekirse, önceki anlık görüntüleri kaldırarak
daha fazla kullanılabilir alan oluşturabilirsiniz:
%1 anlık görüntüler %2 disk alanı kaplıyor.
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="433"/>
        <source>Desktop</source>
        <translation>Masaüstü</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="435"/>
        <source>Documents</source>
        <translation>Belgeler</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="437"/>
        <source>Downloads</source>
        <translation>İndirilenler</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="439"/>
        <source>Music</source>
        <translation>Müzik</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="441"/>
        <source>Networks</source>
        <translation>Ağlar</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="443"/>
        <source>Pictures</source>
        <translation>Resimler</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="447"/>
        <source>Videos</source>
        <translation>Videolar</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="627"/>
        <location filename="settings.cpp" line="717"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation>Çıktı dosyası %1 zaten var. Lütfen başka bir dosya adı kullanın veya mevcut dosyayı silin.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="121"/>
        <source>Interrupted or failed to complete</source>
        <translation>Kesintiye uğradı veya tamamlanamadı</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="88"/>
        <source>Cleaning...</source>
        <translation>Temizleniyor...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="112"/>
        <location filename="work.cpp" line="120"/>
        <location filename="work.cpp" line="276"/>
        <source>Done</source>
        <translation>Bitti</translation>
    </message>
    <message>
        <location filename="work.cpp" line="151"/>
        <location filename="work.cpp" line="233"/>
        <location filename="work.cpp" line="256"/>
        <location filename="work.cpp" line="295"/>
        <location filename="work.cpp" line="411"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="work.cpp" line="152"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>Hedef diskinizde yeterli boş alan yok, en az %1 gerekli</translation>
    </message>
    <message>
        <location filename="work.cpp" line="155"/>
        <source>You have %1 free space on %2</source>
        <translation>%2 üzerinde %1 boş alanınız var</translation>
    </message>
    <message>
        <location filename="work.cpp" line="157"/>
        <source>If you are sure you have enough free space rerun the program with -o/--override-size option</source>
        <translation>Yeterli boş alanınız olduğundan eminseniz, programı -o/--override-size seçeneğiyle yeniden çalıştırın.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="181"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Yeni iso dosya sistemi kopyalanıyor...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="193"/>
        <source>Could not create temp directory. </source>
        <translation>Geçici dizin oluşturulamadı.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="230"/>
        <source>Squashing filesystem...</source>
        <translation>Dosya sistemi sıkıştırılıyor...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="234"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>linuxfs dosyası oluşturulamadı, lütfen hedef bölüm üzerinde yeterli alan olup olmadığını kontrol edin.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="253"/>
        <source>Creating CD/DVD image file...</source>
        <translation>CD/DVD kalıp dosyası oluşturuluyor...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="257"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>ISO dosyası oluşturulamadı, lütfen hedef bölüm üzerinde yeterli alan olup olmadığını kontrol edin.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="263"/>
        <source>Making hybrid iso</source>
        <translation>Hibrit iso oluşturuluyor</translation>
    </message>
    <message>
        <location filename="work.cpp" line="281"/>
        <source>Success</source>
        <translation>Başarılı</translation>
    </message>
    <message>
        <location filename="work.cpp" line="282"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>Anlık görüntü alınması başarıyla tamamlandı!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="283"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Anlık Görüntüyü Oluşturmak %1 sürdü</translation>
    </message>
    <message>
        <location filename="work.cpp" line="284"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>ISO Anlık Görüntüsünü kullandığınız için teşekkürler, sonra Live USB Maker&apos;ı çalıştırın!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="292"/>
        <source>Installing </source>
        <translation>Kuruluyor</translation>
    </message>
    <message>
        <location filename="work.cpp" line="295"/>
        <source>Could not install </source>
        <translation>Kurulamadı</translation>
    </message>
    <message>
        <location filename="work.cpp" line="305"/>
        <source>Calculating checksum...</source>
        <translation>Sağlama hesaplanıyor...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="341"/>
        <source>Building new initrd...</source>
        <translation>Yeni initrd oluşturuluyor...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="412"/>
        <source>Could not create working directory. </source>
        <translation>Çalışma dizini oluşturulamadı.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="558"/>
        <source>Calculating total size of excluded files...</source>
        <translation>Hariç tutulan dosyaların toplam boyutu hesaplanıyor...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="569"/>
        <source>Calculating size of root...</source>
        <translation>Kök boyutu hesaplanıyor...</translation>
    </message>
</context>
</TS>