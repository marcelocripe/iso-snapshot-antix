<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="es_ES">
<context>
    <name>Batchprocessing</name>
    <message>
        <location filename="batchprocessing.cpp" line="44"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="45"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>El kernel actual no admite el algoritmo de compresión seleccionado, edite el archivo de configuración y seleccione un algoritmo diferente.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MX Snapshot</source>
        <translation>MX Snapshot</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Optional customization</source>
        <translation>Personalización opcional</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Boot options:</source>
        <translation>Opciones de arranque:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>Release date:</source>
        <translation>Fecha de publicación:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Project name:</source>
        <translation>Nombre del proyecto:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Release version:</source>
        <translation>Versión de lanzamiento:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <source>Release codename:</source>
        <translation>Nombre clave del proyecto:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot es una utilidad que crea una imagen de arranque (ISO) de su sistema de trabajo que puede usar para almacenamiento o distribución. Puede seguir trabajando con aplicaciones poco exigentes mientras se ejecuta.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Espacio usado en las particiones / (root) y /home:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <source>Location and ISO name</source>
        <translation>Ubicación y nombre de la ISO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="188"/>
        <source>Snapshot location:</source>
        <translation>Ubicación de la imagen:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>Select a different snapshot directory</source>
        <translation>Por favor seleccione un directorio diferente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <location filename="mainwindow.cpp" line="328"/>
        <source>Snapshot name:</source>
        <translation>Nombre de la imagen:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="mainwindow.ui" line="370"/>
        <source>TextLabel</source>
        <translation>Etiqueta de Texto</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /etc/mx-snapshot-exclude.list.</source>
        <translation>También puede excluir ciertos directorios marcando las opciones comunes a continuación, o haciendo clic en el botón para editar directamente /etc/mx-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="395"/>
        <source>Pictures</source>
        <translation>Imágenes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <source>Music</source>
        <translation>Música</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <source>All of the above</source>
        <translation>Todo lo anterior</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="426"/>
        <source>Documents</source>
        <translation>Documentos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <source>Downloads</source>
        <translation>Descargas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <source>Desktop</source>
        <translation>Escritorio</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="447"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <source>exclude network configurations</source>
        <translation>excluir configuraciones de red</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>Networks</source>
        <translation>Redes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="505"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Cuentas preservadas (para el respaldo personal)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="515"/>
        <source>Type of snapshot:</source>
        <translation>Tipo de imagen del sistema:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the MX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esta opción restablecerá las contraseñas &amp;quot;demo&amp;quot; y &amp;quot;root&amp;quot; a los valores predeterminados de MX Linux y no copiará ninguna cuenta personal creada.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="538"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Restablecimiento de cuentas (para distribuir a otros)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>Edit Exclusion File</source>
        <translation>Editar archivo de exclusión</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="577"/>
        <source>sha512</source>
        <translation>sha512</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="584"/>
        <source>Options:</source>
        <translation>Opciones:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="604"/>
        <source>md5</source>
        <translation>md5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="611"/>
        <source>Calculate checksums:</source>
        <translation>Calcular sumas de comprobación:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="618"/>
        <source>ISO compression scheme:</source>
        <translation>Esquema de compresión ISO:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="733"/>
        <source>About this application</source>
        <translation>Acerca de esta aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <source>About...</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="743"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="759"/>
        <source>Quit application</source>
        <translation>Salir de la aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="762"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="769"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="849"/>
        <source>Back</source>
        <translation>Atras</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="866"/>
        <source>Display help </source>
        <translation>Mostrar la ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="869"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="876"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Snapshot</source>
        <translation>Instantanea</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>fastest, worst compression</source>
        <translation>más rápida, peor compresión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>fast, worse compression</source>
        <translation>rápida, peor compresión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>slow, better compression</source>
        <translation>lenta, mejor compresión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>best compromise</source>
        <translation>el mejor compromiso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="166"/>
        <source>slowest, best compression</source>
        <translation>más lento, mejor compresión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="193"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Espacio libre en %1, donde se colocará la imagen del sistema: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>El espacio libre debería ser suficiente para contener los datos comprimidos de / y /home

      Si es necesario, puede crear más espacio disponible
      eliminando imagenes anteriores y copias guardadas:
      %1 imagenes están ocupando %2 del espacio en disco.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <location filename="mainwindow.cpp" line="207"/>
        <source>Installing </source>
        <translation>Instalando</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Please wait.</source>
        <translation>Espere por favor.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Espere por favor. Calculando el espacio usado en el disco...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="312"/>
        <location filename="mainwindow.cpp" line="340"/>
        <location filename="mainwindow.cpp" line="369"/>
        <location filename="mainwindow.cpp" line="373"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="313"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation>El archivo de salida %1 ya existe. Por favor, utilice otro nombre de archivo o elimine el archivo existente.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="320"/>
        <source>Settings</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="325"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Snapshot usará los siguientes ajustes:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="327"/>
        <source>- Snapshot directory:</source>
        <translation>- Directorio de la imagen:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="328"/>
        <source>- Kernel to be used:</source>
        <translation>- Kernel a usar:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="341"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>El kernel actual no admite el algoritmo de compresión seleccionado, edite el archivo de configuración y seleccione un algoritmo diferente.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="348"/>
        <source>Final chance</source>
        <translation>Ultima oportunidad</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="350"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Snapshot ahora tiene toda la información que necesita para crear una ISO desde su sistema en ejecución.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Tomará algún tiempo en finalizar, dependiendo del tamaño del sistema instalado y la capacidad de su computadora.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>OK to start?</source>
        <translation>¿Desea empezar?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Shutdown computer when done.</source>
        <translation>Apagar el ordenador al finalizar.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="369"/>
        <source>Could not create working directory. </source>
        <translation>No se pudo crear el directorio de trabajo. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="373"/>
        <source>Could not create temporary directory. </source>
        <translation>No se pudo crear el directorio temporal. </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="381"/>
        <source>Output</source>
        <translation>Salida</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>Edit Boot Menu</source>
        <translation>Editar el Menú de Arranque</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>El programa ahora se detendrá para permitirle editar cualquier archivo en el directorio de trabajo. Seleccione Sí para editar el menú de arranque o seleccione No para omitir este paso y continuar creando la imagen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <source>About %1</source>
        <translation>Acerca de %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="491"/>
        <source>Version: </source>
        <translation>Versión: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Programa para crear un live-CD del sistema actual de antiX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="496"/>
        <source>%1 License</source>
        <translation>%1 Licencia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="510"/>
        <source>%1 Help</source>
        <translation>%1 Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="518"/>
        <source>Select Snapshot Directory</source>
        <translation>Seleccione el directorio de la imagen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="540"/>
        <source>Confirmation</source>
        <translation>Confirmación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="540"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>¿Está seguro de que desea salir de la aplicación?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <location filename="about.cpp" line="53"/>
        <location filename="about.cpp" line="62"/>
        <source>Changelog</source>
        <translation>Registro de cambios</translation>
    </message>
    <message>
        <location filename="about.cpp" line="54"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>&amp;Cerrar</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="67"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>El programa pausará la compilación y abrirá el menú de inicio en su editor de texto.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Herramienta usada para crear un Live-CD desde el sistema actual</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use CLI only</source>
        <translation>Usar CLI solamente</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>Output directory</source>
        <translation>Directorio de salida</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="100"/>
        <source>path</source>
        <translation>ruta</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Output filename</source>
        <translation>Nombre del archivo de salida</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>name</source>
        <translation>nombre</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Nombre un kernel diferente para usar que no sea el kernel en ejecución predeterminado, use el formato devuelto por &apos;uname -r&apos;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>Or the full path: %1</source>
        <translation>O la ruta completa: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="81"/>
        <source>version, or path</source>
        <translation>versión, o ruta</translation>
    </message>
    <message>
        <location filename="main.cpp" line="83"/>
        <source>Compression level options.</source>
        <translation>Opciones de niveles de compresión.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Use quotes: &quot;-Xcompression-level &lt;level&gt;&quot;, or &quot;-Xalgorithm &lt;algorithm&gt;&quot;, or &quot;-Xhc&quot;, see mksquashfs man page</source>
        <translation>Utilice las comillas: &quot;-Xcompression-level &lt;level&gt;&quot;, o &quot;-Xalgorithm &lt;algorithm&gt;&quot;, o &quot;-Xhc&quot;, vea la página man de mksquashfs</translation>
    </message>
    <message>
        <location filename="main.cpp" line="86"/>
        <source>&quot;option&quot;</source>
        <translation>&quot;opciones&quot;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="89"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Crear una imagen mensual, añadir el &apos;Mes&apos; en el nombre ISO, omitir el cálculo del espacio usado</translation>
    </message>
    <message>
        <location filename="main.cpp" line="90"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Esta opción restablece las cuentas y la compresión a los valores predeterminados, los argumentos que cambian esos elementos se ignorarán</translation>
    </message>
    <message>
        <location filename="main.cpp" line="93"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>No calcule sumas de comprobación para el archivo ISO resultante</translation>
    </message>
    <message>
        <location filename="main.cpp" line="95"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Opción para solucionar el problema con el cálculo de sumas de comprobación en kernels preempt_rt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="96"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Restablecimiento de cuentas (para distribuir a otros)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="97"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Calcular sumas de comprobación para el archivo ISO resultante</translation>
    </message>
    <message>
        <location filename="main.cpp" line="99"/>
        <source>Skip calculating free space to see if the resulting ISO will fit</source>
        <translation>Omitir el cálculo del espacio libre para ver si la ISO resultante cabe</translation>
    </message>
    <message>
        <location filename="main.cpp" line="100"/>
        <source>Work directory</source>
        <translation>Directorio de trabajo</translation>
    </message>
    <message>
        <location filename="main.cpp" line="102"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Excluir carpetas principales, opciones válidas: </translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Use la opción una vez para cada elemento que desee excluir</translation>
    </message>
    <message>
        <location filename="main.cpp" line="105"/>
        <source>one item</source>
        <translation>un elemento</translation>
    </message>
    <message>
        <location filename="main.cpp" line="107"/>
        <source>Compression format, valid choices: </source>
        <translation>Formato de compresión, opciones válidas: </translation>
    </message>
    <message>
        <location filename="main.cpp" line="108"/>
        <source>format</source>
        <translation>formato</translation>
    </message>
    <message>
        <location filename="main.cpp" line="109"/>
        <source>Shutdown computer when done.</source>
        <translation>Apagar el ordenador al finalizar.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="128"/>
        <location filename="main.cpp" line="138"/>
        <location filename="main.cpp" line="176"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Parece que ha iniciado sesión como root, cierre la sesión e inicie sesión como usuario normal para usar este programa.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="150"/>
        <location filename="main.cpp" line="184"/>
        <source>version:</source>
        <translation>versión:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="158"/>
        <source>You must run this program as root.</source>
        <translation>Debe ejecutar este programa como root.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="167"/>
        <source>Snapshot</source>
        <translation>Instantanea</translation>
    </message>
    <message>
        <location filename="main.cpp" line="175"/>
        <location filename="main.cpp" line="259"/>
        <location filename="settings.cpp" line="222"/>
        <location filename="settings.cpp" line="235"/>
        <location filename="settings.cpp" line="633"/>
        <location filename="settings.cpp" line="723"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="main.cpp" line="253"/>
        <location filename="main.cpp" line="255"/>
        <location filename="settings.cpp" line="230"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>El kernel actual no soporta Squashfs, no puede continuar.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>No se pudo crear el directorio de trabajo. </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="109"/>
        <source>Could not create temp directory. </source>
        <translation>No se pudo crear el directorio temporal. </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="217"/>
        <source>Could not find a usable kernel</source>
        <translation>No se pudo encontrar un kernel utilizable</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="346"/>
        <source>Used space on / (root): </source>
        <translation>Espacio usado en / (root): </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>estimated</source>
        <translation>estimado</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="360"/>
        <source>Used space on /home: </source>
        <translation>Espacio usado en /home: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="411"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Espacio libre en %1, donde se colocará la imagen del sistema: </translation>
    </message>
    <message>
        <location filename="settings.cpp" line="415"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>El espacio libre debería ser suficiente para contener los datos comprimidos de / y /home

       Si es necesario, puede crear más espacio disponible
       eliminando imagenes del sistema anteriores y copias guardadas:
       Las imagenes %1 están ocupando %2 del espacio en disco.
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="433"/>
        <source>Desktop</source>
        <translation>Escritorio</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="435"/>
        <source>Documents</source>
        <translation>Documentos</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="437"/>
        <source>Downloads</source>
        <translation>Descargas</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="439"/>
        <source>Music</source>
        <translation>Música</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="441"/>
        <source>Networks</source>
        <translation>Redes</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="443"/>
        <source>Pictures</source>
        <translation>Imágenes</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="447"/>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="627"/>
        <location filename="settings.cpp" line="717"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation>El archivo de salida %1 ya existe. Por favor, utilice otro nombre de archivo o elimine el archivo existente.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="121"/>
        <source>Interrupted or failed to complete</source>
        <translation>Interrumpido o no se pudo completar</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="88"/>
        <source>Cleaning...</source>
        <translation>Limpiando...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="112"/>
        <location filename="work.cpp" line="120"/>
        <location filename="work.cpp" line="276"/>
        <source>Done</source>
        <translation>Hecho</translation>
    </message>
    <message>
        <location filename="work.cpp" line="151"/>
        <location filename="work.cpp" line="233"/>
        <location filename="work.cpp" line="256"/>
        <location filename="work.cpp" line="295"/>
        <location filename="work.cpp" line="411"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="work.cpp" line="152"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>No hay suficiente espacio libre en el disco de destino, necesita al menos %1</translation>
    </message>
    <message>
        <location filename="work.cpp" line="155"/>
        <source>You have %1 free space on %2</source>
        <translation>Tienes %1  de espacio libre en %2</translation>
    </message>
    <message>
        <location filename="work.cpp" line="157"/>
        <source>If you are sure you have enough free space rerun the program with -o/--override-size option</source>
        <translation>Si está seguro de que tiene suficiente espacio libre vuelva a ejecutar el programa con la opción -o/--override-size</translation>
    </message>
    <message>
        <location filename="work.cpp" line="181"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Copiando el sistema de archivos nueva-iso...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="193"/>
        <source>Could not create temp directory. </source>
        <translation>No se pudo crear el directorio temporal. </translation>
    </message>
    <message>
        <location filename="work.cpp" line="230"/>
        <source>Squashing filesystem...</source>
        <translation>Comprimiendo el sistema de archivos...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="234"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>No se pudo crear el archivo linuxfs, verifique si tiene suficiente espacio en la partición de destino.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="253"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Creando un archivo de imagen de CD/DVD...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="257"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>No se pudo crear el archivo ISO, verifique si tiene suficiente espacio en la partición de destino.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="263"/>
        <source>Making hybrid iso</source>
        <translation>Creando un iso híbrido</translation>
    </message>
    <message>
        <location filename="work.cpp" line="281"/>
        <source>Success</source>
        <translation>Correcto</translation>
    </message>
    <message>
        <location filename="work.cpp" line="282"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>¡Instantánea completada con éxito!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="283"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Snapshot tardó %1 en finalizar.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="284"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Gracias por usar ISO Snapshot, ¡ejecute Grabador de USB en vivo a continuación!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="292"/>
        <source>Installing </source>
        <translation>Instalando </translation>
    </message>
    <message>
        <location filename="work.cpp" line="295"/>
        <source>Could not install </source>
        <translation>No se pudo instalar </translation>
    </message>
    <message>
        <location filename="work.cpp" line="305"/>
        <source>Calculating checksum...</source>
        <translation>Calculando suma de comprobación...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="341"/>
        <source>Building new initrd...</source>
        <translation>Construyendo nuevo initrd...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="412"/>
        <source>Could not create working directory. </source>
        <translation>No se pudo crear el directorio de trabajo. </translation>
    </message>
    <message>
        <location filename="work.cpp" line="558"/>
        <source>Calculating total size of excluded files...</source>
        <translation>Calculando el tamaño total de los archivos excluidos...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="569"/>
        <source>Calculating size of root...</source>
        <translation>Calculando el tamaño de root...</translation>
    </message>
</context>
</TS>