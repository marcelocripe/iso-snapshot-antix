<?xml version="1.0" ?><!DOCTYPE TS><TS version="2.1" language="ru">
<context>
    <name>Batchprocessing</name>
    <message>
        <location filename="batchprocessing.cpp" line="44"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="45"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Текущее ядро не поддерживает выбранный алгоритм сжатия, пожалуйста, измените конфигурационный файл и выберите другой алгоритм.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MX Snapshot</source>
        <translation>MX Снимок системы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Optional customization</source>
        <translation>Дополнительная настройка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="43"/>
        <source>Boot options:</source>
        <translation>Параметры загрузки:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="70"/>
        <source>Release date:</source>
        <translation>Дата выпуска:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="91"/>
        <source>Project name:</source>
        <translation>Имя проекта:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>Release version:</source>
        <translation>Версия выпуска:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="112"/>
        <source>Release codename:</source>
        <translation>Кодовое имя выпуска:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Snapshot is a utility that creates a bootable image (ISO) of your working system that you can use for storage or distribution. You can continue working with undemanding applications while it is running.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Утилита Snapshot создает загрузочный образ (ISO) Вашей рабочей системы, который можно использовать для хранения или распространения. Вы можете продолжать работать с нетребовательными приложениями во время его использования.&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="151"/>
        <source>Used space on / (root) and /home partitions:</source>
        <translation>Используемое место на / (корневом) и домашнем разделах:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="161"/>
        <source>Location and ISO name</source>
        <translation>Расположение и название ISO</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="188"/>
        <source>Snapshot location:</source>
        <translation>Расположение снимков:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>Select a different snapshot directory</source>
        <translation>Выбрать другую папку для снимка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="206"/>
        <location filename="mainwindow.cpp" line="328"/>
        <source>Snapshot name:</source>
        <translation>Имя снимка:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <location filename="mainwindow.ui" line="370"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="338"/>
        <source>You can also exclude certain directories by ticking the common choices below, or by clicking on the button to directly edit /etc/mx-snapshot-exclude.list.</source>
        <translation>Вы также можете исключить определенные папки, отметив их ниже, или нажать на кнопку, чтобы напрямую изменить /etc/mx-snapshot-exclude.list.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="395"/>
        <source>Pictures</source>
        <translation>Изображения</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <source>Music</source>
        <translation>Музыка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="419"/>
        <source>All of the above</source>
        <translation>Все вышеперечисленное</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="426"/>
        <source>Documents</source>
        <translation>Документы</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="433"/>
        <source>Downloads</source>
        <translation>Загрузки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="440"/>
        <source>Desktop</source>
        <translation>Рабочий стол</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="447"/>
        <source>Videos</source>
        <translation>Видео</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="464"/>
        <source>exclude network configurations</source>
        <translation>исключить настройки сетей</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="467"/>
        <source>Networks</source>
        <translation>Сети</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="505"/>
        <source>Preserving accounts (for personal backup)</source>
        <translation>Сохранение учетных записей (для личного резервного копирования)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="515"/>
        <source>Type of snapshot:</source>
        <translation>Тип снимка:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="535"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will reset &amp;quot;demo&amp;quot; and &amp;quot;root&amp;quot; passwords to the MX Linux defaults and will not copy any personal accounts created.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Эта опция сбросит &amp;quot;demo&amp;quot; и &amp;quot;root&amp;quot; пароли MX Linux по умолчанию и не будет копировать какие-либо созданные персональные учетные записи.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="538"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Сброс учетных записей (дистрибутив для других)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>Edit Exclusion File</source>
        <translation>Править файл исключений</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="577"/>
        <source>sha512</source>
        <translation>sha512</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="584"/>
        <source>Options:</source>
        <translation>Опции:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="604"/>
        <source>md5</source>
        <translation>md5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="611"/>
        <source>Calculate checksums:</source>
        <translation>Вычислить контрольные суммы:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="618"/>
        <source>ISO compression scheme:</source>
        <translation>Методика сжатия ISO:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="733"/>
        <source>About this application</source>
        <translation>Об этом приложении</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="736"/>
        <source>About...</source>
        <translation>О программе...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="743"/>
        <source>Alt+B</source>
        <translation>Alt+B</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="759"/>
        <source>Quit application</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="762"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="769"/>
        <source>Alt+N</source>
        <translation>Alt+N</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <source>Next</source>
        <translation>Дальше</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="849"/>
        <source>Back</source>
        <translation>Назад</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="866"/>
        <source>Display help </source>
        <translation>Показать справку</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="869"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="876"/>
        <source>Alt+H</source>
        <translation>Alt+H</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Snapshot</source>
        <translation>Снимок системы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>fastest, worst compression</source>
        <translation>самое быстрое, худшее сжатие</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="164"/>
        <source>fast, worse compression</source>
        <translation>быстрое, хуже сжатие</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>slow, better compression</source>
        <translation>медленное, лучше сжатие</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="165"/>
        <source>best compromise</source>
        <translation>наилучший компромисс</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="166"/>
        <source>slowest, best compression</source>
        <translation>самое медленное, наилучшее сжатие</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="193"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Свободное место на %1, где будет папка со снимком:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="196"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Свободного места должно быть достаточно для сжатых данных из / и /home

Если нужно, Вы можете создать больше доступного места,
удалив предыдущие снимки и сохраненные копии:
%1 снимков занимают %2 дискового пространства.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <location filename="mainwindow.cpp" line="207"/>
        <source>Installing </source>
        <translation>Установка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="297"/>
        <source>Please wait.</source>
        <translation>Пожалуйста, ждите.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="299"/>
        <source>Please wait. Calculating used disk space...</source>
        <translation>Пожалуйста, ждите. Расчет используемого дискового пространства...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="312"/>
        <location filename="mainwindow.cpp" line="340"/>
        <location filename="mainwindow.cpp" line="369"/>
        <location filename="mainwindow.cpp" line="373"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="313"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation>Выходной файл %1 уже существует. Пожалуйста, используйте другое имя файла или удалите существующий файл.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="320"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="325"/>
        <source>Snapshot will use the following settings:*</source>
        <translation>Снимок будет использовать следующие параметры:*</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="327"/>
        <source>- Snapshot directory:</source>
        <translation>- Папка снимков:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="328"/>
        <source>- Kernel to be used:</source>
        <translation>- Используемое ядро:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="341"/>
        <source>Current kernel doesn&apos;t support selected compression algorithm, please edit the configuration file and select a different algorithm.</source>
        <translation>Текущее ядро не поддерживает выбранный алгоритм сжатия, пожалуйста, измените конфигурационный файл и выберите другой алгоритм.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="348"/>
        <source>Final chance</source>
        <translation>Последний шанс</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="350"/>
        <source>Snapshot now has all the information it needs to create an ISO from your running system.</source>
        <translation>Имеется вся информация Снимка, необходимая для создания ISO из рабочей системы.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>It will take some time to finish, depending on the size of the installed system and the capacity of your computer.</source>
        <translation>Это займёт некоторое время, в зависимости от размера установленной системы и способностей вашего компьютера.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>OK to start?</source>
        <translation>OK для старта?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Shutdown computer when done.</source>
        <translation>Выключить компьютер по завершении.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="369"/>
        <source>Could not create working directory. </source>
        <translation>Не удалось создать рабочий каталог.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="373"/>
        <source>Could not create temporary directory. </source>
        <translation>Не удалось создать временный каталог.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="381"/>
        <source>Output</source>
        <translation>Вывод</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="393"/>
        <source>Edit Boot Menu</source>
        <translation>Править меню загрузки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>The program will now pause to allow you to edit any files in the work directory. Select Yes to edit the boot menu or select No to bypass this step and continue creating the snapshot.</source>
        <translation>Теперь программа будет приостановлена, чтобы позволить вам править файлы в рабочем каталоге. Выберите Да, чтобы изменить меню загрузки, или выберите Нет, чтобы пропустить этот шаг и продолжить создание образа.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <source>About %1</source>
        <translation>О %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="491"/>
        <source>Version: </source>
        <translation>Версия: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Program for creating a live-CD from the running system for antiX Linux</source>
        <translation>Программа для создания Live-CD из работающей системы для Linux Antix</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="494"/>
        <source>Copyright (c) MX Linux</source>
        <translation>Copyright (c) MX Linux</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="496"/>
        <source>%1 License</source>
        <translation>%1 Лицензия</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="510"/>
        <source>%1 Help</source>
        <translation>%1 Справка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="518"/>
        <source>Select Snapshot Directory</source>
        <translation>Выберите папку для снимков</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="540"/>
        <source>Confirmation</source>
        <translation>Подтверждение</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="540"/>
        <source>Are you sure you want to quit the application?</source>
        <translation>Вы уверены, что хотите выйти из приложения?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="about.cpp" line="52"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="about.cpp" line="53"/>
        <location filename="about.cpp" line="62"/>
        <source>Changelog</source>
        <translation>Список изменений</translation>
    </message>
    <message>
        <location filename="about.cpp" line="54"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="about.cpp" line="74"/>
        <source>&amp;Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="batchprocessing.cpp" line="67"/>
        <source>The program will pause the build and open the boot menu in your text editor.</source>
        <translation>Программа приостановит сборку и откроет меню загрузки в вашем текстовом редакторе.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="69"/>
        <source>Tool used for creating a live-CD from the running system</source>
        <translation>Средство создания live-CD из работающей системы</translation>
    </message>
    <message>
        <location filename="main.cpp" line="73"/>
        <source>Use CLI only</source>
        <translation>Использовать только интерфейс командной строки</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <source>Output directory</source>
        <translation>Выходной каталог</translation>
    </message>
    <message>
        <location filename="main.cpp" line="75"/>
        <location filename="main.cpp" line="100"/>
        <source>path</source>
        <translation>путь</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>Output filename</source>
        <translation>Имя выходного файла</translation>
    </message>
    <message>
        <location filename="main.cpp" line="76"/>
        <source>name</source>
        <translation>имя</translation>
    </message>
    <message>
        <location filename="main.cpp" line="78"/>
        <source>Name a different kernel to use other than the default running kernel, use format returned by &apos;uname -r&apos;</source>
        <translation>Назовите для использования другое ядро, отличное от запущенного по умолчанию, используйте формат, возвращаемый «uname -r»</translation>
    </message>
    <message>
        <location filename="main.cpp" line="80"/>
        <source>Or the full path: %1</source>
        <translation>Или полный путь: %1</translation>
    </message>
    <message>
        <location filename="main.cpp" line="81"/>
        <source>version, or path</source>
        <translation>версия или путь</translation>
    </message>
    <message>
        <location filename="main.cpp" line="83"/>
        <source>Compression level options.</source>
        <translation>Параметры уровня сжатия.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="84"/>
        <source>Use quotes: &quot;-Xcompression-level &lt;level&gt;&quot;, or &quot;-Xalgorithm &lt;algorithm&gt;&quot;, or &quot;-Xhc&quot;, see mksquashfs man page</source>
        <translation>Используйте кавычки: &quot;-Xcompression-level &lt;level&gt;&quot;, &quot;-Xalgorithm &lt;algorithm&gt;&quot; или &quot;-Xhc&quot;; смотрите справочную страницу mksquashfs</translation>
    </message>
    <message>
        <location filename="main.cpp" line="86"/>
        <source>&quot;option&quot;</source>
        <translation>&quot;параметр&quot;</translation>
    </message>
    <message>
        <location filename="main.cpp" line="89"/>
        <source>Create a monthly snapshot, add &apos;Month&apos; name in the ISO name, skip used space calculation</source>
        <translation>Создать ежемесячный снимок, добавить название месяца в имя ISO, пропустить вычисление используемого места</translation>
    </message>
    <message>
        <location filename="main.cpp" line="90"/>
        <source>This option sets reset-accounts and compression to defaults, arguments changing those items will be ignored</source>
        <translation>Этот параметр устанавливает значения reset-accounts и compression по умолчанию, аргументы, меняющие эти записи, будут игнорироваться</translation>
    </message>
    <message>
        <location filename="main.cpp" line="93"/>
        <source>Don&apos;t calculate checksums for resulting ISO file</source>
        <translation>Не вычислять контрольные суммы для конечного ISO-файла</translation>
    </message>
    <message>
        <location filename="main.cpp" line="95"/>
        <source>Option to fix issue with calculating checksums on preempt_rt kernels</source>
        <translation>Параметр для устранения проблемы с вычислением контрольных сумм в ядрах preempt_rt</translation>
    </message>
    <message>
        <location filename="main.cpp" line="96"/>
        <source>Resetting accounts (for distribution to others)</source>
        <translation>Сброс учетных записей (дистрибутив для других)</translation>
    </message>
    <message>
        <location filename="main.cpp" line="97"/>
        <source>Calculate checksums for resulting ISO file</source>
        <translation>Вычисление контрольных сумм для конечного ISO-файла</translation>
    </message>
    <message>
        <location filename="main.cpp" line="99"/>
        <source>Skip calculating free space to see if the resulting ISO will fit</source>
        <translation>Пропустить вычисление свободного места, чтобы посмотреть, поместится ли результирующий ISO</translation>
    </message>
    <message>
        <location filename="main.cpp" line="100"/>
        <source>Work directory</source>
        <translation>Рабочий каталог</translation>
    </message>
    <message>
        <location filename="main.cpp" line="102"/>
        <source>Exclude main folders, valid choices: </source>
        <translation>Исключить основные папки, допустимые варианты:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="104"/>
        <source>Use the option one time for each item you want to exclude</source>
        <translation>Используйте этот параметр один раз для каждого объекта, который вы хотите исключить</translation>
    </message>
    <message>
        <location filename="main.cpp" line="105"/>
        <source>one item</source>
        <translation>один элемент</translation>
    </message>
    <message>
        <location filename="main.cpp" line="107"/>
        <source>Compression format, valid choices: </source>
        <translation>Формат сжатия, допустимые варианты:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="108"/>
        <source>format</source>
        <translation>формат</translation>
    </message>
    <message>
        <location filename="main.cpp" line="109"/>
        <source>Shutdown computer when done.</source>
        <translation>Выключить компьютер по завершении.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="128"/>
        <location filename="main.cpp" line="138"/>
        <location filename="main.cpp" line="176"/>
        <source>You seem to be logged in as root, please log out and log in as normal user to use this program.</source>
        <translation>Программа запущена суперпользователем. Для использования программы войдите в систему как обычный пользователь.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="150"/>
        <location filename="main.cpp" line="184"/>
        <source>version:</source>
        <translation>версия:</translation>
    </message>
    <message>
        <location filename="main.cpp" line="158"/>
        <source>You must run this program as root.</source>
        <translation>Вы должны запустить программу от имени суперпользователя.</translation>
    </message>
    <message>
        <location filename="main.cpp" line="167"/>
        <source>Snapshot</source>
        <translation>Снимок системы</translation>
    </message>
    <message>
        <location filename="main.cpp" line="175"/>
        <location filename="main.cpp" line="259"/>
        <location filename="settings.cpp" line="222"/>
        <location filename="settings.cpp" line="235"/>
        <location filename="settings.cpp" line="633"/>
        <location filename="settings.cpp" line="723"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="main.cpp" line="253"/>
        <location filename="main.cpp" line="255"/>
        <location filename="settings.cpp" line="230"/>
        <source>Current kernel doesn&apos;t support Squashfs, cannot continue.</source>
        <translation>Текущее ядро не поддерживает Squashfs, невозможно продолжить.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="87"/>
        <source>Could not create working directory. </source>
        <translation>Не удалось создать рабочий каталог.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="109"/>
        <source>Could not create temp directory. </source>
        <translation>Не удалось создать временный каталог.</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="217"/>
        <source>Could not find a usable kernel</source>
        <translation>Не удалось найти пригодное для использования ядро</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="346"/>
        <source>Used space on / (root): </source>
        <translation>Используемое место на / (корневом) разделе:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="349"/>
        <source>estimated</source>
        <translation>предполагаемый</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="360"/>
        <source>Used space on /home: </source>
        <translation>Используемое место на домашнем разделе:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="411"/>
        <source>Free space on %1, where snapshot folder is placed: </source>
        <translation>Свободное место на %1, где будет папка со снимком:</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="415"/>
        <source>The free space should be sufficient to hold the compressed data from / and /home

      If necessary, you can create more available space
      by removing previous snapshots and saved copies:
      %1 snapshots are taking up %2 of disk space.
</source>
        <translation>Свободного места должно быть достаточно для сжатых данных из / и /home

Если нужно, Вы можете создать больше доступного места,
удалив предыдущие снимки и сохраненные копии:
%1 снимков занимают %2 дискового пространства.
</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="433"/>
        <source>Desktop</source>
        <translation>Рабочий стол</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="435"/>
        <source>Documents</source>
        <translation>Документы</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="437"/>
        <source>Downloads</source>
        <translation>Загрузки</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="439"/>
        <source>Music</source>
        <translation>Музыка</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="441"/>
        <source>Networks</source>
        <translation>Сети</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="443"/>
        <source>Pictures</source>
        <translation>Изображения</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="447"/>
        <source>Videos</source>
        <translation>Видео</translation>
    </message>
    <message>
        <location filename="settings.cpp" line="627"/>
        <location filename="settings.cpp" line="717"/>
        <source>Output file %1 already exists. Please use another file name, or delete the existent file.</source>
        <translation>Выходной файл %1 уже существует. Пожалуйста, используйте другое имя файла или удалите существующий файл.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="121"/>
        <source>Interrupted or failed to complete</source>
        <translation>Прервано или не удалось завершить</translation>
    </message>
</context>
<context>
    <name>Work</name>
    <message>
        <location filename="work.cpp" line="88"/>
        <source>Cleaning...</source>
        <translation>Очистка...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="112"/>
        <location filename="work.cpp" line="120"/>
        <location filename="work.cpp" line="276"/>
        <source>Done</source>
        <translation>Готово</translation>
    </message>
    <message>
        <location filename="work.cpp" line="151"/>
        <location filename="work.cpp" line="233"/>
        <location filename="work.cpp" line="256"/>
        <location filename="work.cpp" line="295"/>
        <location filename="work.cpp" line="411"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="work.cpp" line="152"/>
        <source>There&apos;s not enough free space on your target disk, you need at least %1</source>
        <translation>На вашем целевом диске недостаточно свободного места, нужно как минимум %1</translation>
    </message>
    <message>
        <location filename="work.cpp" line="155"/>
        <source>You have %1 free space on %2</source>
        <translation>У вас свободно %1 на %2</translation>
    </message>
    <message>
        <location filename="work.cpp" line="157"/>
        <source>If you are sure you have enough free space rerun the program with -o/--override-size option</source>
        <translation>Если вы уверены, что у вас достаточно свободного места, повторно запустите программу с параметром «-o/--override-size»</translation>
    </message>
    <message>
        <location filename="work.cpp" line="181"/>
        <source>Copying the new-iso filesystem...</source>
        <translation>Копирование новой файловой системы ISO...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="193"/>
        <source>Could not create temp directory. </source>
        <translation>Не удалось создать временный каталог.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="230"/>
        <source>Squashing filesystem...</source>
        <translation>Сжатие файловой системы...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="234"/>
        <source>Could not create linuxfs file, please check whether you have enough space on the destination partition.</source>
        <translation>Не удалось создать файл linuxfs, пожалуйста, проверьте, есть ли у Вас достаточно места на целевом разделе.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="253"/>
        <source>Creating CD/DVD image file...</source>
        <translation>Создание файла образа CD/DVD...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="257"/>
        <source>Could not create ISO file, please check whether you have enough space on the destination partition.</source>
        <translation>Не удалось создать ISO-файл, пожалуйста, проверьте, есть ли у вас достаточно места на целевом разделе.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="263"/>
        <source>Making hybrid iso</source>
        <translation>Создание гибридного ISO</translation>
    </message>
    <message>
        <location filename="work.cpp" line="281"/>
        <source>Success</source>
        <translation>Успешно</translation>
    </message>
    <message>
        <location filename="work.cpp" line="282"/>
        <source>Snapshot completed sucessfully!</source>
        <translation>Снимок успешно завершён!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="283"/>
        <source>Snapshot took %1 to finish.</source>
        <translation>Создание снимка завершено за %1.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="284"/>
        <source>Thanks for using ISO Snapshot, run Live USB Maker next!</source>
        <translation>Спасибо за использование снимка ISO, далее запустите Live USB Maker!</translation>
    </message>
    <message>
        <location filename="work.cpp" line="292"/>
        <source>Installing </source>
        <translation>Установка</translation>
    </message>
    <message>
        <location filename="work.cpp" line="295"/>
        <source>Could not install </source>
        <translation>Не удалось установить</translation>
    </message>
    <message>
        <location filename="work.cpp" line="305"/>
        <source>Calculating checksum...</source>
        <translation>Вычисление контрольной суммы...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="341"/>
        <source>Building new initrd...</source>
        <translation>Формирую новый initrd...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="412"/>
        <source>Could not create working directory. </source>
        <translation>Не удалось создать рабочий каталог.</translation>
    </message>
    <message>
        <location filename="work.cpp" line="558"/>
        <source>Calculating total size of excluded files...</source>
        <translation>Вычисление общего размера исключённых файлов...</translation>
    </message>
    <message>
        <location filename="work.cpp" line="569"/>
        <source>Calculating size of root...</source>
        <translation>Вычисление размера root...</translation>
    </message>
</context>
</TS>